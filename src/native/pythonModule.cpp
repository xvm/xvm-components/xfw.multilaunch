// SPDX-License-Identifier: MIT
// Copyright (c) 2020-2024 XVM Contributors

//
// Includes
//

// windows
#include <Windows.h>
#include <process.h>

// pybind11
#include <pybind11/pybind11.h>



//
// Defines
//

#define NT_SUCCESS(x) ((x) >= 0)
#define STATUS_INFO_LENGTH_MISMATCH 0xc0000004

#define SystemHandleInformation 16
#define ObjectBasicInformation 0
#define ObjectNameInformation 1
#define ObjectTypeInformation 2



//
// Typedefs
//

typedef NTSTATUS(NTAPI *_NtQuerySystemInformation)(
    ULONG SystemInformationClass,
    PVOID SystemInformation,
    ULONG SystemInformationLength,
    PULONG ReturnLength
    );

typedef NTSTATUS(NTAPI *_NtDuplicateObject)(
    HANDLE SourceProcessHandle,
    HANDLE SourceHandle,
    HANDLE TargetProcessHandle,
    PHANDLE TargetHandle,
    ACCESS_MASK DesiredAccess,
    ULONG Attributes,
    ULONG Options
    );

typedef NTSTATUS(NTAPI *_NtQueryObject)(
    HANDLE ObjectHandle,
    ULONG ObjectInformationClass,
    PVOID ObjectInformation,
    ULONG ObjectInformationLength,
    PULONG ReturnLength
    );

typedef struct _UNICODE_STRING
{
    USHORT Length;
    USHORT MaximumLength;
    PWSTR Buffer;
} UNICODE_STRING, *PUNICODE_STRING;

typedef struct _SYSTEM_HANDLE
{
    ULONG ProcessId;
    BYTE ObjectTypeNumber;
    BYTE Flags;
    USHORT Handle;
    PVOID Object;
    ACCESS_MASK GrantedAccess;
} SYSTEM_HANDLE, *PSYSTEM_HANDLE;

typedef struct _SYSTEM_HANDLE_INFORMATION
{
    ULONG HandleCount;
    SYSTEM_HANDLE Handles[1];
} SYSTEM_HANDLE_INFORMATION, *PSYSTEM_HANDLE_INFORMATION;

typedef enum _POOL_TYPE
{
    NonPagedPool,
    PagedPool,
    NonPagedPoolMustSucceed,
    DontUseThisType,
    NonPagedPoolCacheAligned,
    PagedPoolCacheAligned,
    NonPagedPoolCacheAlignedMustS
} POOL_TYPE, *PPOOL_TYPE;

typedef struct _OBJECT_TYPE_INFORMATION
{
    UNICODE_STRING Name;
    ULONG TotalNumberOfObjects;
    ULONG TotalNumberOfHandles;
    ULONG TotalPagedPoolUsage;
    ULONG TotalNonPagedPoolUsage;
    ULONG TotalNamePoolUsage;
    ULONG TotalHandleTableUsage;
    ULONG HighWaterNumberOfObjects;
    ULONG HighWaterNumberOfHandles;
    ULONG HighWaterPagedPoolUsage;
    ULONG HighWaterNonPagedPoolUsage;
    ULONG HighWaterNamePoolUsage;
    ULONG HighWaterHandleTableUsage;
    ULONG InvalidAttributes;
    GENERIC_MAPPING GenericMapping;
    ULONG ValidAccess;
    BOOLEAN SecurityRequired;
    BOOLEAN MaintainHandleCount;
    USHORT MaintainTypeList;
    POOL_TYPE PoolType;
    ULONG PagedPoolUsage;
    ULONG NonPagedPoolUsage;
} OBJECT_TYPE_INFORMATION, *POBJECT_TYPE_INFORMATION;



//
// Helpers
//

void* GetLibraryProcAddress(const char* LibraryName, const char* ProcName)
{
    return GetProcAddress(GetModuleHandleA(LibraryName), ProcName);
}

bool IsTargetMutex(PCWSTR mutex_name) {
    const std::vector<const wchar_t*> target_names = {
        // WG
        L"wot_client_mutex",
        L"wgc_game_mtx_",
        L"wgc_running_games_mtx",
        // LESTA
        L"lgc_game_mtx_",
        L"lgc_running_games_mtx",
    };

    for (const auto* target_name : target_names) {
        if (wcsstr(mutex_name, target_name)) {
            return true;
        }
    }
    
    return false;
}



//
// Public
//

bool kill_mutex()
{
    auto* NtQuerySystemInformation = reinterpret_cast<_NtQuerySystemInformation>(GetLibraryProcAddress("ntdll.dll", "NtQuerySystemInformation"));
    auto* NtDuplicateObject = reinterpret_cast<_NtDuplicateObject>(GetLibraryProcAddress("ntdll.dll", "NtDuplicateObject"));
    auto* NtQueryObject = reinterpret_cast<_NtQueryObject>(GetLibraryProcAddress("ntdll.dll", "NtQueryObject"));

    if(!NtQuerySystemInformation || !NtDuplicateObject || !NtQueryObject){
        return false;
    }

    NTSTATUS status;
    PSYSTEM_HANDLE_INFORMATION handleInfo;
    ULONG handleInfoSize = 0x10000;
    bool flag = false;

    HANDLE processHandle = GetCurrentProcess();
    handleInfo = (PSYSTEM_HANDLE_INFORMATION)malloc(handleInfoSize);

    /* NtQuerySystemInformation won't give us the correct buffer size, so we guess by doubling the buffer size. */
    while ((status = NtQuerySystemInformation(SystemHandleInformation, handleInfo, handleInfoSize, NULL)) == STATUS_INFO_LENGTH_MISMATCH)
    {
        handleInfo = (PSYSTEM_HANDLE_INFORMATION)realloc(handleInfo, handleInfoSize *= 2);
    }

    /* NtQuerySystemInformation stopped giving us STATUS_INFO_LENGTH_MISMATCH. */
    if (!NT_SUCCESS(status)){
        free(handleInfo);
        return false;
    }

    for (int i = 0; i < handleInfo->HandleCount; i++)
    {
        SYSTEM_HANDLE handle = handleInfo->Handles[i];
        POBJECT_TYPE_INFORMATION objectTypeInfo;
        PVOID objectNameInfo;
        UNICODE_STRING objectName;
        ULONG returnLength;

        /* Query the object type. */
        objectTypeInfo = (POBJECT_TYPE_INFORMATION)malloc(0x1000);
        if (!NT_SUCCESS(NtQueryObject((HANDLE)handle.Handle, ObjectTypeInformation, objectTypeInfo, 0x1000, NULL)))
        {
            continue;
        }

        /* Query the object name (unless it has an access of 0x0012019f, on which NtQueryObject could hang. */
        if (handle.GrantedAccess == 0x0012019f)
        {
            free(objectTypeInfo);
            continue;
        }

        /* Fix crash on Wine */
        if (objectTypeInfo->Name.Buffer == NULL)
        {
            free(objectTypeInfo);
            continue;
        }

        if (wcscmp(objectTypeInfo->Name.Buffer, L"Mutant") == 0)
        {
            objectNameInfo = malloc(0x1000);
            if (!NT_SUCCESS(NtQueryObject((HANDLE)handle.Handle, ObjectNameInformation, objectNameInfo, 0x1000, &returnLength)))
            {
                /* Reallocate the buffer and try again. */
                objectNameInfo = realloc(objectNameInfo, returnLength);
                if (!NT_SUCCESS(NtQueryObject((HANDLE)handle.Handle, ObjectNameInformation, objectNameInfo, returnLength, NULL)))
                {
                    free(objectTypeInfo);
                    free(objectNameInfo);
                    continue;
                }
            }

            /* Cast our buffer into an UNICODE_STRING. */
            objectName = *(PUNICODE_STRING)objectNameInfo;

            if (objectName.Length)
            {
                if (IsTargetMutex(objectName.Buffer))
                {
                    CloseHandle((HANDLE)handle.Handle);
                    flag = true;
                }
            }
            free(objectNameInfo);
        }
        free(objectTypeInfo);
    }

    free(handleInfo);
    CloseHandle(processHandle);

    return flag;
}



//
// Module
//

PYBIND11_MODULE(XFW_Multilaunch, m) {
    m.doc() = "XFW Multilaunch module";
    m.def("kill_mutex", &kill_mutex, pybind11::call_guard<pybind11::gil_scoped_release>());
}
