"""
SPDX-License-Identifier: MIT
Copyright (c) 2020-2022 XVM Contributors
"""

#
# Imports
#

# cpython
import logging


# xfw.native
from xfw_native.python import XFWNativeModuleWrapper



#
# Globals
#

g_xfw_multilaunch = None



#
# Public
#

def kill_mutex():
    global g_xfw_multilaunch
    if not g_xfw_multilaunch:
        return None
    return g_xfw_multilaunch.kill_mutex()



#
# XFW Loader
#

def xfw_is_module_loaded():
    global g_xfw_multilaunch
    return g_xfw_multilaunch is not None


def xfw_module_init():
    global g_xfw_multilaunch
    g_xfw_multilaunch = XFWNativeModuleWrapper('com.modxvm.xfw.multilaunch', 'xfw_multilaunch.pyd', 'XFW_Multilaunch')


def xfw_module_fini():
    global g_xfw_multilaunch
    g_xfw_multilaunch = None

